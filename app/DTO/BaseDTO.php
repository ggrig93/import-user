<?php

namespace App\DTO;

abstract class BaseDTO
{

    /**
     * @return array
     */
    public abstract function toArray(): array;

}
