<?php

namespace App\DTO;


class UserDTO extends BaseDTO
{
    /**
     * @param string $firstname
     * @param string $lastname
     * @param string $email
     * @param string $age
     */
    private function __construct(
        public readonly string $firstname,
        public readonly string $lastname,
        public readonly string $email,
        public readonly string $age) {}

    /**
     * @param string $firstname
     * @param string $lastname
     * @param string $email
     * @param string|null $age
     * @return UserDTO
     */
    public static function create(string $firstname, string $lastname, string $email, string $age = null): UserDTO
    {
        return new self($firstname, $lastname, $email, $age);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {

        return [
            'first_name' => trim($this->firstname),
            'last_name' => trim($this->lastname),
            'email' => $this->email,
            'age' => $this->age,
        ];
    }

}
