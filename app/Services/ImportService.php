<?php

namespace App\Services;

use App\DTO\UserDTO;
use App\Interfaces\ImportService as UserImportInterface;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class ImportService implements UserImportInterface
{

    const BATCH_SIZE = 100;
    const LIMIT = 5000;

    /**
     * @param array $data
     * @return array
     */
    public function import(array $data): array
    {
        $batchSize = $data['batchSize'] ?? self::BATCH_SIZE;
        $response = Http::get(config('app.user_import_api'), ['results' => self::LIMIT])->json();

        if(isset($response['error'])) {
            return $response;
        }
        $dataChunk = array_chunk($response['results'], $batchSize);
        $updatedUserCount = 0;
        $createUserCount = 0;
        $error = '';

        foreach ($dataChunk as $chunk) {
            DB::beginTransaction();
            $users = [];
            $fullNames = [];

            try {
                foreach ($chunk as $user) {

                    if(!isset($user['name']['first']) || !isset($user['name']['last']) || !isset($user['dob']['age']) ){
                        continue;
                    }
                    $userData = UserDTO::create(
                        $user['name']['first'],
                        $user['name']['last'],
                        $user['email'],
                        $user['dob']['age']
                    )->toArray();
                    // get array of users key by user full name
                    $fullName = "{$userData['first_name']} {$userData['last_name']}";
                    $users["$fullName"] = $userData;
                    $fullNames[] = $fullName;
                }

                $fullNames = array_unique($fullNames);
                //find all user full names that should be updated.
                $existingUserFullNames = User::query()->select(DB::raw("CONVERT(CONCAT(first_name, ' ', last_name) USING utf8mb4) as full_name"))
                    ->whereIn(DB::raw("CONVERT(CONCAT(first_name, ' ', last_name) USING utf8mb4)"), $fullNames)
                    ->get()
                    ->pluck('full_name')
                    ->toArray();

                $existingUserFullNames = array_unique($existingUserFullNames);
//                map through existing user full names and update records
                foreach ($existingUserFullNames as $fullName) {
                    if(!isset($users[$fullName])) {
                        continue;
                    }
                    $user = $users[$fullName];
                    //remove existing user from main list for batch creation
                    unset($users[$fullName]);

                   User::query()
                        ->where('first_name', $user['first_name'])
                        ->where('last_name', $user['last_name'])
                        ->update($user);
                }
                //Upsert for additional check there can be names like Aleksa Srejović and Aleksa Srejovic mysql default settings understand both same
                DB::table('users')->upsert($users,['first_name', 'last_name'], ['age', 'email']);
//                User::insert($users);
                DB::commit();
                $createUserCount += count($users);
                $updatedUserCount += count($existingUserFullNames);

            } catch (\Exception $e) {
                Log::info($e->getMessage() . 'on line - '. $e->getLine());
                $error = $e->getMessage();
                DB::rollBack();
            }
        }

        $response = [
            'created' => $createUserCount,
            'updated' => $updatedUserCount,
            'all' => User::query()->count('id'),
        ];

        if($error) {
            $response['error'] = $error;
        }

        return $response;
    }
}
