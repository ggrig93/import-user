<?php

namespace App\Providers;

use App\Interfaces\ImportService as UserImportInterface;
use App\Services\ImportService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->app->bind(UserImportInterface::class,ImportService::class);
    }
}
