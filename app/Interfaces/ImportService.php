<?php

namespace App\Interfaces;

interface ImportService
{
    /**
     * @param array $data
     * @return array
     */
    public function import(array $data): array;
}
