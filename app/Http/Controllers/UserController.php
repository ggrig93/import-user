<?php

namespace App\Http\Controllers;


use App\Interfaces\ImportService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @param ImportService $importService
     * @return array
     */
    public function import(Request $request, ImportService $importService): array
    {
        $data = $request->all();

        return $importService->import($data);
    }
}
