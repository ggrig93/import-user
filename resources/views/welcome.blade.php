<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <!-- Styles -->
    <style>
        .btn-custom {
            background: lightblue;
            padding: 10px 18px;
            border: unset;
            border-radius: 4px;
            cursor: pointer;
        }

        .main {
            margin-top: 50px;
        }
        .mt-20 {
            margin-top: 20px;
        }
        .d-none {
            display: none;
        }
    </style>
</head>
<body>
<div class="main">
    <div>
        <button class="btn btn-custom import"> Import users</button>
    </div>
    <div class="counts mt-20 d-none">
        created: <span data-type="created"> 0 </span>
        <br>
        updated: <span data-type="updated" > 0 </span>
        <br>
        all: <span data-type="all"> 0 </span>
    </div>
    <div class="error-block d-none">

    </div>
</div>

<script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function () {

        $(".import").on('click', function () {
            $('.counts').addClass('d-none');
            $('.error-block ').addClass('d-none');
            $.ajax({
                url: "/import",
                success: function (result) {
                    if(result['error']) {
                        $('.error-block').removeClass('d-none')
                        $('.error-block').text(result['error'])
                    }

                    $('.counts').removeClass('d-none');
                    $("[data-type='created']").text(result['created']);
                    $("[data-type='updated']").text(result['updated']);
                    $("[data-type='all']").text(result['all']);
                }
            });
        });

    });
</script>
</body>
</html>
